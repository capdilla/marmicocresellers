<?php


namespace PrestaShop\Module\MarmicocResellers\Models;

use ObjectModel;


class ResellerProductModel extends ObjectModel
{

    public static $definition = [
        'table' => 'product_reseller',
        'primary' => 'id',
        'fields' => [
            'id_reseller' => ['type' => self::TYPE_INT],
            'id_product' => ['type' => self::TYPE_INT],
            'url' => ['type' => self::TYPE_STRING],
        ]
    ];
}
