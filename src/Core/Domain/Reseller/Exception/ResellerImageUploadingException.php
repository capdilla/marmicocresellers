<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Exception;

/**
 * Is thrown when error occurs when uploading language image
 */
class ResellerImageUploadingException extends ResellerException
{
    /**
     * @var int Code is used when there are less memory than needed to upload image
     */
    const MEMORY_LIMIT_RESTRICTION = 1;

    /**
     * @var int Code is used when unexpected error occurs while uploading image
     */
    const UNEXPECTED_ERROR = 2;

    /**
     * Code is used when image/images cannot be resized
     */
    const UNABLE_RESIZE = 3;
}
