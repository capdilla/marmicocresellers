<?php
/**
 * @author    Alejandro del Valle <a.delvalle@softec.cloud>
 * @copyright Marmicoc
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */

namespace PrestaShop\Module\MarmicocResellers\Core\Grid\Query;

use Doctrine\DBAL\Query\QueryBuilder;
use PrestaShop\PrestaShop\Core\Grid\Query\AbstractDoctrineQueryBuilder;
use PrestaShop\PrestaShop\Core\Grid\Search\SearchCriteriaInterface;

/**
 * Class MarmicookMemberQueryBuilder.
 */
final class MarmicookMemberQueryBuilder extends AbstractDoctrineQueryBuilder
{
    /**
     * @param null|SearchCriteriaInterface $searchCriteria
     *
     * @return QueryBuilder
     */
    public function getSearchQueryBuilder(SearchCriteriaInterface $searchCriteria)
    {
        $qb = $this->getQueryBuilder($searchCriteria->getFilters());
        $qb->select('m.*, c.firstname, c.lastname, c.id_customer');
        $qb->orderBy(
            $searchCriteria->getOrderBy(),
            $searchCriteria->getOrderWay()
        );

        if ($searchCriteria->getLimit() > 0) {
            $qb->setFirstResult($searchCriteria->getOffset());
            $qb->setMaxResults($searchCriteria->getLimit());
        }

        return $qb;
    }

    /**
     * @param null|SearchCriteriaInterface $searchCriteria
     *
     * @return QueryBuilder
     */
    public function getCountQueryBuilder(SearchCriteriaInterface $searchCriteria)
    {
        $qb = $this->getQueryBuilder($searchCriteria->getFilters());
        $qb->select('COUNT(m.id_member)');

        return $qb;
    }

    /**
     * Get generic query builder.
     *
     * @param array $filters
     *
     * @return QueryBuilder
     */
    private function getQueryBuilder(array $filters)
    {
        $qb = $this->connection
            ->createQueryBuilder()
            ->from($this->dbPrefix . 'marmicoc_member', 'm')
            ->leftJoin('m', $this->dbPrefix . 'customer', 'c', 'm.id_customer = c.id_customer');

        foreach ($filters as $name => $value) {
            if ('id_lang' === $name) {
                continue;
            }

            if ('id_member' === $name) {
                $qb
                    ->andWhere("m.id_member = :$name")
                    ->setParameter($name, $value)
                ;
                continue;
            }

            if ('firstname' === $name) {
                $qb
                    ->andWhere("c.firstname = :$name")
                    ->setParameter($name, $value)
                ;
                continue;
            }

            if ('lastname' === $name) {
                $qb
                    ->andWhere("c.lastname = :$name")
                    ->setParameter($name, $value)
                ;
                continue;
            }

            $qb
                ->andWhere(sprintf('m.%s LIKE :%s', $name, $name))
                ->setParameter($name, '%' . $value . '%')
            ;
        }

        return $qb;
    }
}
