<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Command;

use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Exception\ResellerConstraintException;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\ValueObject\ResellerId;

/**
 * Deletes resellers in bulk action
 */
class BulkDeleteResellerCommand
{
    /**
     * @var ResellerId[]
     */
    private $resellerIds;

    /**
     * @param int[] $resellerIds
     *
     * @throws ResellerConstraintException
     */
    public function __construct(array $resellerIds)
    {
        $this->setResellerIds($resellerIds);
    }

    /**
     * @return ResellerId[]
     */
    public function getResellerIds()
    {
        return $this->resellerIds;
    }

    /**
     * @param array $resellerIds
     *
     * @throws ResellerConstraintException
     */
    private function setResellerIds(array $resellerIds)
    {
        foreach ($resellerIds as $resellerId) {
            $this->resellerIds[] = new ResellerId($resellerId);
        }
    }
}
