<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once __DIR__ . '/vendor/autoload.php';

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;
use PrestaShop\PrestaShop\Adapter\SymfonyContainer;
use PrestaShop\PrestaShop\Adapter\LegacyContext;
use PrestaShop\PrestaShop\Adapter\Shop\Context;
use Doctrine\ORM\EntityManagerInterface;



class MarmicocResellers extends Module
{
    public function __construct()
    {
        $this->name = 'marmicocresellers';
        $this->author = 'Alejandro del Valle';
        $this->version = '1.0.0';
        $this->need_instance = 0;
        $this->tab = 'front_office_features';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Marmicoc Resellers');
        $this->description = $this->l('Módulo para la gestión de Revendedores');
        $this->secure_key = Tools::encrypt($this->name);

        $this->ps_versions_compliancy = array('min' => '1.7.5.0', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        return parent::install()
            && $this->addTables()
            // && $this->installTab('AdminCatalog', 'AdminMarmicookReseller', 'Revendedores')
            && $this->registerHook(
                array(
                    'actionAdminControllerSetMedia',
                    'displayProductAdditionalInfo',
                    'displayAdminProductsExtra'
                )
            );
    }

    public function uninstall()
    {
        return parent::uninstall()
            && $this->uninstallTab('AdminMarmicookReseller');
    }

    public function enable($force_all = false)
    {

        return parent::enable($force_all)
            && $this->installTab('AdminParentManufacturers', 'AdminMarmicookReseller', 'Revendedores');
    }

    public function disable($force_all = false)
    {
        return parent::disable($force_all)
            && $this->uninstallTab('AdminMarmicookReseller');
    }

    /**
     * The Core is supposed to register the tabs automatically thanks to the getTabs() return.
     * However in 1.7.5 it only works when the module contains a AdminLinkWidgetController file,
     * this works fine when module has been upgraded and the former file is still present however
     * for a fresh install we need to install it manually until the core is able to manage new modules.
     *
     * @return bool
     */
    public function installTab($parent, $className, $name)
    {
        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = $className;
        $tab->id_parent = is_null($parent) ? 0 : (int)Tab::getIdFromClassName($parent);
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = $name;
        }
        $tab->module = $this->name;
        return $tab->save();
    }

    public function uninstallTab($className)
    {
        $tabId = Tab::getIdFromClassName($className);
        $tab = new Tab($tabId);
        return $tab->delete();
    }

    public function hookActionAdminControllerSetMedia($params)
    {

        if ($this->context->controller->controller_name == 'AdminProducts') {
            $this->context->controller->addCSS(_PS_CSS_DIR_ . 'jquery.fancybox.css', 'screen');
            $this->context->controller->addJqueryPlugin('fancybox');
            $this->context->controller->addJS($this->_path . 'views/js/resellers/resellers_product_extra.js');
        }
    }

    // Adding a configuration page
    // https://devdocs.prestashop.com/1.7/modules/creation/adding-configuration-page/

    public function hookDisplayAdminProductsExtra($params)
    {
        $id_product = Tools::getValue('id_product', isset($params['id_product']) ? $params['id_product'] : null);
        $product = new Product((int) $id_product);

        if (!Validate::isLoadedObject($product)) {
            return (false);
        }

        $ajax_link = $this->context->link->getModuleLink(
            'marmicocresellers',
            'ajax',
            [
                'secure_key' => $this->secure_key,
                'id_product' => $id_product
            ]

        );

        $this->context->smarty->assign(
            array(
                'ajax_link' => $ajax_link
            )
        );

        return $this->display(__FILE__, './views/templates/admin/product/resellers.tpl');
    }

    public function hookDisplayProductAdditionalInfo($params)
    {
        // Product info
        $id_product = (int)$params['product']['id'];
        $id_product_attribute = $params['product']['id_product_attribute'];

        $query = new DbQuery();

        $query->select("*");
        $query->from('product_reseller', 'pr');
        $query->rightJoin('marmicoc_reseller', 'rs', 'rs.id_reseller = pr.id_reseller');
        $query->where('active = 1 and id_product=' . $id_product);

        $resellers = Db::getInstance()->executeS($query);

        $resellers = array_map(function ($reseller) {
            $reseller['logo'] = _PS_BASE_URL_ . __PS_BASE_URI__ . 'modules/' . $reseller['logo'];
            return $reseller;
        }, $resellers);

        $this->context->smarty->assign(
            array(
                'resellers' => $resellers,
            )
        );

        return $this->display(__FILE__, 'views/templates/hook/product_resellers.tpl');
    }



    private function addTables()
    {
        DB::getInstance()->execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'marmicoc_reseller` (
            `id_reseller` INT(11) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
            `logo` VARCHAR(500) NOT NULL,
            `name` VARCHAR(255) NOT NULL,
            `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
            `date_add` DATETIME NOT NULL,
            `date_upd` DATETIME NOT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');

        return DB::getInstance()->execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'product_reseller` (
            `id` INT(11) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
            `id_reseller` INT(11) unsigned NOT NULL,
            `id_product` INT(11) unsigned NOT NULL,
            `url` VARCHAR(255) NOT NULL,
            `date_add` DATETIME NOT NULL,
            `date_upd` DATETIME NOT NULL,

            CONSTRAINT `' . _DB_PREFIX_ . 'product_reseller_id_reseller` 
            FOREIGN KEY (`id_reseller`) 
            REFERENCES `' . _DB_PREFIX_ . 'marmicoc_reseller`(`id_reseller`) 
            ON DELETE CASCADE ON UPDATE CASCADE,

            CONSTRAINT `' . _DB_PREFIX_ . '_product_reseller_id_product`
            FOREIGN KEY (`id_product`) REFERENCES `' . _DB_PREFIX_ . 'product`(`id_product`) 
            ON DELETE CASCADE ON UPDATE CASCADE

        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
    }
}
