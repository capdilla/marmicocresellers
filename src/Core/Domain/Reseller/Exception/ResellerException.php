<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Exception;

use PrestaShop\PrestaShop\Core\Domain\Exception\DomainException;

/**
 * Base exception for Reseller subdomain
 */
class ResellerException extends DomainException
{
}
