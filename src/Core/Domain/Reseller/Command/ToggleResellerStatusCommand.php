<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Command;

use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Exception\ResellerConstraintException;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\ValueObject\ResellerId;

/**
 * Toggles reseller status
 */
class ToggleResellerStatusCommand
{
    /**
     * @var ResellerId
     */
    private $resellerId;

    /**
     * @var bool
     */
    private $expectedStatus;

    /**
     * @param int $resellerId
     * @param bool $expectedStatus
     *
     * @throws ResellerConstraintException
     */
    public function __construct($resellerId, $expectedStatus)
    {
        $this->assertIsBool($expectedStatus);
        $this->expectedStatus = $expectedStatus;
        $this->resellerId = new ResellerId($resellerId);
    }

    /**
     * @return ResellerId
     */
    public function getResellerId()
    {
        return $this->resellerId;
    }

    /**
     * @return bool
     */
    public function getExpectedStatus()
    {
        return $this->expectedStatus;
    }

    /**
     * Validates that value is of type boolean
     *
     * @param $value
     *
     * @throws ResellerConstraintException
     */
    private function assertIsBool($value)
    {
        if (!is_bool($value)) {
            throw new ResellerConstraintException(
                sprintf('Status must be of type bool, but given %s', var_export($value, true)),
                ResellerConstraintException::INVALID_STATUS
            );
        }
    }
}
