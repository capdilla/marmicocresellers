<?php
/**
 * @author    Alejandro del Valle <a.delvalle@softec.cloud>
 * @copyright 2018 Marmicoc
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */

namespace PrestaShop\Module\MarmicocResellers\Core\Grid\Definition\Factory;

use PrestaShop\PrestaShop\Core\Grid\Action\GridActionCollection;
use PrestaShop\PrestaShop\Core\Grid\Action\Type\SimpleGridAction;
use PrestaShop\PrestaShop\Core\Grid\Action\Type\LinkGridAction;
use PrestaShop\PrestaShop\Core\Grid\Action\Bulk\BulkActionCollection;
use PrestaShop\PrestaShop\Core\Grid\Action\Bulk\Type\SubmitBulkAction;
//use PrestaShop\PrestaShop\Core\Grid\Action\Row\RowActionCollection;
//use PrestaShop\PrestaShop\Core\Grid\Action\Row\Type\LinkRowAction;
//use PrestaShop\PrestaShop\Core\Grid\Action\Row\Type\SubmitRowAction;
use PrestaShop\PrestaShop\Core\Grid\Column\ColumnCollection;
use PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\ActionColumn;
use PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\BulkActionColumn;
use PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\ToggleColumn;
use PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\DateTimeColumn;
use PrestaShop\PrestaShop\Core\Grid\Column\Type\DataColumn;
use PrestaShop\PrestaShop\Core\Grid\Filter\Filter;
use PrestaShop\PrestaShop\Core\Grid\Filter\FilterCollection;
use PrestaShopBundle\Form\Admin\Type\SearchAndResetType;
use PrestaShop\PrestaShop\Core\Grid\Definition\Factory\AbstractGridDefinitionFactory;
use PrestaShop\PrestaShop\Core\Hook\HookDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class MarmicookMemberGridDefinitionFactory.
 */
final class MarmicookMemberGridDefinitionFactory extends AbstractGridDefinitionFactory
{
    const GRID_ID = 'member';

    /**
     * @var string
     */
    private $resetActionUrl;

    /**
     * @var string
     */
    private $redirectionUrl;

    /**
     * MarmicookMemberGridDefinitionFactory constructor.
     *
     * @param HookDispatcherInterface $hookDispatcher
     * @param string $resetActionUrl
     * @param string $redirectionUrl
     */
    public function __construct(
        HookDispatcherInterface $hookDispatcher,
        $resetActionUrl,
        $redirectionUrl
    ) {
        parent::__construct($hookDispatcher);

        $this->resetActionUrl = $resetActionUrl;
        $this->redirectionUrl = $redirectionUrl;
    }

    /**
     * {@inheritdoc}
     */
    protected function getId()
    {
        return self::GRID_ID;
    }

    /**
     * {@inheritdoc}
     */
    protected function getName()
    {
        return $this->trans('Listado de miembros', [], 'Modules.Marmicook.Admin');
    }

    /**
     * {@inheritdoc}
     */
    protected function getColumns()
    {
        return (new ColumnCollection())
            ->add(
                (new BulkActionColumn('bulk'))
                ->setOptions([
                    'bulk_field' => 'id_member',
                ])
            )
            ->add(
                (new DataColumn('id_member'))
                ->setName($this->trans('ID', [], 'Modules.Marmicook.Admin'))
                ->setOptions([
                    'field' => 'id_member',
                ])
            )
            ->add(
                (new DataColumn('id_customer'))
                ->setName($this->trans('ID de cliente', [], 'Modules.Marmicook.Admin'))
                ->setOptions([
                    'field' => 'id_customer',
                ])
            )
            ->add(
                (new DataColumn('firstname'))
                ->setName($this->trans('Nombre', [], 'Modules.Marmicook.Admin'))
                ->setOptions([
                    'field' => 'firstname',
                ])
            )
            ->add(
                (new DataColumn('lastname'))
                ->setName($this->trans('Apellidos', [], 'Modules.Marmicook.Admin'))
                ->setOptions([
                    'field' => 'lastname',
                ])
            )
            ->add(
                (new DateTimeColumn('date_add'))
                ->setName($this->trans('Fecha de creación', [], 'Modules.Marmicook.Admin'))
                ->setOptions([
                    'field' => 'date_add',
                ])
            )
            ->add(
                (new DateTimeColumn('date_upd'))
                ->setName($this->trans('Fecha de actualización', [], 'Modules.Marmicook.Admin'))
                ->setOptions([
                    'field' => 'date_upd',
                ])
            )
            ->add(
                (new ToggleColumn('status'))
                ->setName($this->trans('Activo', [], 'Modules.Marmicook.Admin'))
                ->setOptions([
                    'field' => 'status',
                    'primary_field' => 'id_member',
                    'route' => 'admin_marmicoc_members_toggle_status',
                    'route_param_id' => 'memberId',
                ])
            )
            ->add(
                (new ActionColumn('actions'))
                ->setName($this->trans('Acciones', [], 'Modules.Marmicook.Admin'))
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function getFilters()
    {
        return (new FilterCollection())
            ->add(
                (new Filter('id_member', TextType::class))
                ->setTypeOptions([
                    'required' => false,
                ])
                ->setAssociatedColumn('id_member')
            )
            ->add(
                (new Filter('id_customer', TextType::class))
                ->setTypeOptions([
                    'required' => false,
                ])
                ->setAssociatedColumn('id_customer')
            )
            ->add(
                (new Filter('firstname', TextType::class))
                ->setTypeOptions([
                    'required' => false,
                ])
                ->setAssociatedColumn('firstname')
            )
            ->add(
                (new Filter('lastname', TextType::class))
                ->setTypeOptions([
                    'required' => false,
                ])
                ->setAssociatedColumn('lastname')
            )
            ->add(
                (new Filter('date_add', TextType::class))
                ->setTypeOptions([
                    'required' => false,
                ])
                ->setAssociatedColumn('date_add')
            )
            ->add(
                (new Filter('date_upd', TextType::class))
                ->setTypeOptions([
                    'required' => false,
                ])
                ->setAssociatedColumn('date_upd')
            )
            ->add(
                (new Filter('status', ChoiceType::class))
                ->setTypeOptions([
                    'required' => false,
                    'choices' => array('Sí' => true, 'No' => false),
                ])
                ->setAssociatedColumn('status')
            )
            ->add(
                (new Filter('actions', SearchAndResetType::class))
                ->setTypeOptions([
                    'reset_route' => 'admin_common_reset_search',
                    'reset_route_params' => [
                        'controller' => 'MarmicookMember',
                        'action' => 'index',
                    ],
                    'redirect_route' => 'admin_marmicoc_members_index',
                ])
                ->setAssociatedColumn('actions')
            )
        ;
    }


    /**
     * {@inheritdoc}
     */
    protected function getGridActions()
    {
        return (new GridActionCollection())
            // ->add(
            //     (new LinkGridAction('export'))
            //     ->setName($this->trans('Exportar CSV', [], 'Modules.Marmicook.Admin'))
            //     ->setIcon('cloud_download')
            //     ->setOptions([
            //         'route' => 'admin_marmicoc_members_export',
            //     ])
            // )
            ->add(
                (new SimpleGridAction('common_refresh_list'))
                ->setName($this->trans('Refrescar listado', [], 'Modules.Marmicook.Admin'))
                ->setIcon('refresh')
            )
            ->add(
                (new SimpleGridAction('common_show_query'))
                ->setName($this->trans('Mostrar consulta SQL', [], 'Modules.Marmicook.Admin'))
                ->setIcon('code')
            )
            ->add(
                (new SimpleGridAction('common_export_sql_manager'))
                ->setName($this->trans('Exportar al gestor SQL', [], 'Modules.Marmicook.Admin'))
                ->setIcon('storage')
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function getBulkActions()
    {
        return (new BulkActionCollection())
            ->add(
                (new SubmitBulkAction('update_requests_status'))
                ->setName($this->trans('Actualizar estado', [], 'Modules.Marmicook.Admin'))
                ->setOptions([
                    'submit_route' => 'admin_marmicoc_members_toggle_status_bulk',
                    'confirm_message' => $this->trans('Desea actualizar todas los miembros?', [], 'Modules.Marmicook.Admin'),
                ])
            )
        ;
    }
}
