<?php
/**
 * @author    Alejandro del Valle <a.delvalle@softec.cloud>
 * @copyright Marmicoc
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */

namespace PrestaShop\Module\MarmicocResellers\Core\Search\Filters;

use PrestaShop\PrestaShop\Core\Search\Filters;

/**
 * Class MarmicookMemberFilters.
 */
final class MarmicookMemberFilters extends Filters
{
    /**
     * {@inheritdoc}
     */
    public static function getDefaults()
    {
        return [
            'limit' => 50,
            'offset' => 0,
            'orderBy' => 'id_member',
            'sortOrder' => 'asc',
            'filters' => [],
        ];
    }
}
