<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Member;

use PrestaShopBundle\Component\CsvResponse;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Used to export list of members in CSV.
 * For internal use only.
 */
final class MemberCsvExporter implements MemberExporterInterface
{
    /**
    * @var TranslatorInterface
    */
    private $translator;

    public function __construct(TranslatorInterface $translator, $connection, $dbPrefix)
    {
        $this->translator = $translator;
        $this->connection = $connection;
        $this->dbPrefix = $dbPrefix;
    }

    /**
    *
    * @param array $members
    *
    * @return CsvResponse
    *
    * @throws \InvalidArgumentException
    * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
    */
    public function export(array $members = [])
    {
        $qb = $this->connection->createQueryBuilder();
        $qb->select('m.id_member, c.id_customer, c.firstname, c.lastname, c.email, m.code, m.status');
        $qb->from($this->dbPrefix . 'marmicoc_member', 'm');
        $qb->leftJoin('m', $this->dbPrefix . 'customer', 'c', 'm.id_customer = c.id_customer');
        $qb->orderBy('m.id_member');

        $data = $qb->execute()->fetchAll();

        $header = array(
      'id_member' => 'ID',
      'id_customer' => 'ID Cliente',
      'firstname' => 'Nombre',
      'lastname' => 'Apellidos',
      'email' => 'Email',
      'code' => 'Código',
      'status' => 'Estado'
    );

        return (new CsvResponse())
      ->setData($data)
      ->setHeadersData($header)
      ->setModeType(CsvResponse::MODE_OFFSET)
      ->setLimit(5000)
      ->setFileName('members_' . date('YmdHis') . '.csv');
    }

    /**
    * Translator helper.
    *
    * @param $key
    * @param $domain
    *
    * @return string
    *
    * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
    */
    private function trans($key, $domain)
    {
        return $this->translator->trans($key, array(), $domain);
    }
}
