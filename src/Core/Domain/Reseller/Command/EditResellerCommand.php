<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Command;

use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\ValueObject\ResellerId;

/**
 * Edits reseller with provided data
 */
class EditResellerCommand
{
    /**
     * @var ResellerId
     */
    private $resellerId;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var bool|null
     */
    private $enabled;

    /**
     * @var array|null
     */
    private $associatedShops;

    /**
     * @param int $resellerId
     */
    public function __construct($resellerId)
    {
        $this->resellerId = new ResellerId($resellerId);
    }

    /**
     * @return ResellerId
     */
    public function getResellerId()
    {
        return $this->resellerId;
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool|null $enabled
     *
     * @return self
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return array
     */
    public function getAssociatedShops()
    {
        return $this->associatedShops;
    }

    /**
     * @param $associatedShops
     *
     * @return self
     */
    public function setAssociatedShops($associatedShops)
    {
        $this->associatedShops = $associatedShops;

        return $this;
    }
}
