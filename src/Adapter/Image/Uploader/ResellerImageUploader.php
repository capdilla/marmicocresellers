<?php

namespace PrestaShop\Module\MarmicocResellers\Adapter\Image\Uploader;

use Configuration;
use Context;
use ImageManager;
use ImageType;
use PrestaShop\PrestaShop\Core\Image\Uploader\Exception\ImageOptimizationException;
use PrestaShop\PrestaShop\Core\Image\Uploader\Exception\ImageUploadException;
use PrestaShop\PrestaShop\Core\Image\Uploader\Exception\MemoryLimitException;
use PrestaShopException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use PrestaShop\PrestaShop\Adapter\Image\Uploader\AbstractImageUploader;

/**
 * Uploads reseller logo image
 */
final class ResellerImageUploader extends AbstractImageUploader
{

    const MODULE_IMG_PATH = 'marmicocresellers/views/img/r/';
    const MARMICOOK_RESELLER_IMG_DIR = _PS_MODULE_DIR_ . self::MODULE_IMG_PATH;

    /**
     * {@inheritdoc}
     * @return string
     */
    public function upload($resellerId, UploadedFile $image)
    {
        $this->checkImageIsAllowedForUpload($image);

        $temporaryImageName = tempnam(_PS_TMP_IMG_DIR_, 'PS');

        if (!$temporaryImageName) {
            throw new ImageUploadException(
                'An error occurred while uploading the image. Check your directory permissions.'
            );
        }

        if (!move_uploaded_file($image->getPathname(), $temporaryImageName)) {
            throw new ImageUploadException(
                'An error occurred while uploading the image. Check your directory permissions.'
            );
        }

        // Evaluate the memory required to resize the image: if it's too much, you can't resize it.
        if (!ImageManager::checkImageMemoryLimit($temporaryImageName)) {
            throw new MemoryLimitException(
                'Due to memory limit restrictions, this image cannot be loaded. Increase your memory_limit value.'
            );
        }

        $fileName = self::MODULE_IMG_PATH . $resellerId . '.jpg';

        $fileNamePATH = self::MARMICOOK_RESELLER_IMG_DIR . $resellerId . '.jpg';
        // Copy new image
        if (!ImageManager::resize($temporaryImageName, $fileNamePATH)) {
            throw new ImageOptimizationException(
                'An error occurred while uploading the image. Check your directory permissions.'
            );
        }

        // $this->generateDifferentSizeImages($resellerId);

        return $fileName;
    }

    /**
     * @param int $resellerId
     *
     * @return bool
     */
    private function generateDifferentSizeImages($resellerId)
    {
        $resized = true;
        $generateHighDpiImages = (bool) Configuration::get('PS_HIGHT_DPI');

        try {
            /* Generate images with different size */
            if (
                isset($_FILES) &&
                count($_FILES) &&
                file_exists(self::MARMICOOK_RESELLER_IMG_DIR . $resellerId . '.jpg')
            ) {
                $resized &= ImageManager::resize(
                    self::MARMICOOK_RESELLER_IMG_DIR . $resellerId . '.jpg',
                    self::MARMICOOK_RESELLER_IMG_DIR . $resellerId . '-reseller.jpg',
                    125,
                    125
                );

                if ($generateHighDpiImages) {
                    $resized &= ImageManager::resize(
                        self::MARMICOOK_RESELLER_IMG_DIR . $resellerId . '.jpg',
                        self::MARMICOOK_RESELLER_IMG_DIR . $resellerId . '-reseller2x.jpg',
                        250,
                        250
                    );
                }

                $currentLogo = _PS_TMP_IMG_DIR_ . 'reseller_mini_' . $resellerId .
                    '_' .
                    Context::getContext()->shop->id .
                    '.jpg';

                if ($resized && file_exists($currentLogo)) {
                    unlink($currentLogo);
                }
            }
        } catch (PrestaShopException $e) {
            throw new ImageOptimizationException('Unable to resize one or more of your pictures.');
        }

        if (!$resized) {
            throw new ImageOptimizationException(
                'Unable to resize one or more of your pictures.'
            );
        }

        return $resized;
    }
}
