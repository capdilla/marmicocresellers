<?php

namespace PrestaShop\Module\MarmicocResellers\Adapter\Kpi;

use HelperKpi;
use PrestaShop\PrestaShop\Core\ConfigurationInterface;
use PrestaShop\PrestaShop\Core\Kpi\KpiInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Renders number of how many active resellers.
 */
final class ActiveResellersKpi implements KpiInterface
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var ConfigurationInterface
     */
    private $kpiConfiguration;

    /**
     * @var string
     */
    private $sourceUrl;

    /**
     * @param TranslatorInterface $translator
     * @param ConfigurationInterface $kpiConfiguration
     * @param string $sourceUrl
     */
    public function __construct(
        TranslatorInterface $translator,
        ConfigurationInterface $kpiConfiguration,
        $sourceUrl
    ) {
        $this->translator = $translator;
        $this->kpiConfiguration = $kpiConfiguration;
        $this->sourceUrl = $sourceUrl;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $helper = new HelperKpi();
        $helper->id = 'box-resellers';
        $helper->icon = 'persons';
        $helper->color = 'color3';

        $helper->title = $this->translator->trans('Active resellers', [], 'Modules.Marmicook.Admin');
        $helper->subtitle = $this->translator->trans('All Time', [], 'Admin.Global');

        if (false !== $this->kpiConfiguration->get('MARMICOOK_ACTIVE_RESELLER')) {
            $helper->value = $this->kpiConfiguration->get('MARMICOOK_ACTIVE_RESELLER');
        }

        $helper->source = $this->sourceUrl;
        $helper->refresh = $this->kpiConfiguration->get('MARMICOOK_ACTIVE_RESELLER_EXPIRE') < time();

        return $helper->generate();
    }
}
