
import Grid from "../../../../../Admin/themes/new-theme/js/components/grid/grid";
import ReloadListActionExtension from "../../../../../Admin/themes/new-theme/js/components/grid/extension/reload-list-extension";
import ExportToSqlManagerExtension from "../../../../../Admin/themes/new-theme/js/components/grid/extension/export-to-sql-manager-extension";
import FiltersResetExtension from "../../../../../Admin/themes/new-theme/js/components/grid/extension/filters-reset-extension";
import SortingExtension from "../../../../../Admin/themes/new-theme/js/components/grid/extension/sorting-extension";
import LinkRowActionExtension from "../../../../../Admin/themes/new-theme/js/components/grid/extension/link-row-action-extension";
import SubmitGridExtension from "../../../../../Admin/themes/new-theme/js/components/grid/extension/submit-grid-action-extension";
import SubmitBulkExtension from "../../../../../Admin/themes/new-theme/js/components/grid/extension/submit-bulk-action-extension";
import BulkActionCheckboxExtension from "../../../../../Admin/themes/new-theme/js/components/grid/extension/bulk-action-checkbox-extension";
import SubmitRowActionExtension from "../../../../../Admin/themes/new-theme/js/components/grid/extension/action/row/submit-row-action-extension";
import AsyncToggleColumnExtension from "../../../../../Admin/themes/new-theme/js/components/grid/extension/column/common/async-toggle-column-extension";

const $ = window.$;

$(() => {
  const marmicocResellers = new Grid('reseller');
  marmicocResellers.addExtension(new ReloadListActionExtension());
  marmicocResellers.addExtension(new ExportToSqlManagerExtension());
  marmicocResellers.addExtension(new FiltersResetExtension());
  marmicocResellers.addExtension(new SortingExtension());
  marmicocResellers.addExtension(new LinkRowActionExtension());
  marmicocResellers.addExtension(new SubmitGridExtension());
  marmicocResellers.addExtension(new SubmitBulkExtension());
  marmicocResellers.addExtension(new SubmitRowActionExtension());
  marmicocResellers.addExtension(new BulkActionCheckboxExtension());
  marmicocResellers.addExtension(new AsyncToggleColumnExtension());
});