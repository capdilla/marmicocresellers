<?php
/**
 * @author    Alejandro del Valle <a.delvalle@softec.cloud>
 * @copyright 2018 Marmicoc
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */

namespace PrestaShop\Module\MarmicocResellers\Core\Grid\Definition\Factory;

use PrestaShop\PrestaShop\Core\Grid\Action\GridActionCollection;
use PrestaShop\PrestaShop\Core\Grid\Action\Type\SimpleGridAction;
use PrestaShop\PrestaShop\Core\Grid\Action\Type\LinkGridAction;
use PrestaShop\PrestaShop\Core\Grid\Action\Bulk\BulkActionCollection;
use PrestaShop\PrestaShop\Core\Grid\Action\Bulk\Type\SubmitBulkAction;
use PrestaShop\PrestaShop\Core\Grid\Action\Row\RowActionCollection;
use PrestaShop\PrestaShop\Core\Grid\Action\Row\Type\LinkRowAction;
use PrestaShop\PrestaShop\Core\Grid\Action\Row\Type\SubmitRowAction;
use PrestaShop\PrestaShop\Core\Grid\Column\ColumnCollection;
use PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\ActionColumn;
use PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\BulkActionColumn;
use PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\ToggleColumn;
use PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\DateTimeColumn;
use PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\ImageColumn;
use PrestaShop\PrestaShop\Core\Grid\Column\Type\DataColumn;
use PrestaShop\PrestaShop\Core\Grid\Filter\Filter;
use PrestaShop\PrestaShop\Core\Grid\Filter\FilterCollection;
use PrestaShopBundle\Form\Admin\Type\SearchAndResetType;
use PrestaShop\PrestaShop\Core\Grid\Definition\Factory\AbstractGridDefinitionFactory;
use PrestaShop\PrestaShop\Core\Hook\HookDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class MarmicookResellerGridDefinitionFactory.
 */
final class MarmicookResellerGridDefinitionFactory extends AbstractGridDefinitionFactory
{
    const GRID_ID = 'reseller';

    /**
     * MarmicookResellerGridDefinitionFactory constructor.
     *
     * @param HookDispatcherInterface $hookDispatcher
     */
    public function __construct(
        HookDispatcherInterface $hookDispatcher
    ) {
        parent::__construct($hookDispatcher);
    }

    /**
     * {@inheritdoc}
     */
    protected function getId()
    {
        return self::GRID_ID;
    }

    /**
     * {@inheritdoc}
     */
    protected function getName()
    {
        return $this->trans('Listado de revendedores', [], 'Modules.Marmicook.Admin');
    }

    /**
     * {@inheritdoc}
     */
    protected function getColumns()
    {
        return (new ColumnCollection())
            ->add(
                (new BulkActionColumn('bulk'))
                ->setOptions([
                    'bulk_field' => 'id_reseller',
                ])
            )
            ->add(
                (new DataColumn('id_reseller'))
                ->setName($this->trans('ID', [], 'Modules.Marmicook.Admin'))
                ->setOptions([
                    'field' => 'id_reseller',
                ])
            )
            ->add(
                (new ImageColumn('logo'))
                ->setName($this->trans('Logo', [], 'Modules.Marmicook.Admin'))
                ->setOptions([
                    'src_field' => 'logo',
                ])
            )
            ->add(
                (new DataColumn('name'))
                ->setName($this->trans('Nombre', [], 'Modules.Marmicook.Admin'))
                ->setOptions([
                    'field' => 'name',
                ])
            )
            ->add(
                (new ToggleColumn('active'))
                ->setName($this->trans('Enabled', [], 'Admin.Global'))
                ->setOptions([
                    'field' => 'active',
                    'primary_field' => 'id_reseller',
                    'route' => 'admin_marmicoc_resellers_toggle_status',
                    'route_param_id' => 'resellerId',
                ])
            )
            ->add(
                (new ActionColumn('actions'))
                ->setName($this->trans('Actions', [], 'Admin.Global'))
                ->setOptions([
                    'actions' => (new RowActionCollection())
                        ->add(
                            (new LinkRowAction('edit'))
                            ->setName($this->trans('Edit', [], 'Admin.Actions'))
                            ->setIcon('edit')
                            ->setOptions([
                                'route' => 'admin_marmicoc_resellers_edit',
                                'route_param_name' => 'resellerId',
                                'route_param_field' => 'id_reseller',
                            ])
                        )
                        ->add(
                            (new SubmitRowAction('delete'))
                            ->setName($this->trans('Delete', [], 'Admin.Actions'))
                            ->setIcon('delete')
                            ->setOptions([
                                'route' => 'admin_marmicoc_resellers_delete',
                                'route_param_name' => 'resellerId',
                                'route_param_field' => 'id_reseller',
                                'confirm_message' => $this->trans(
                                    'Delete selected item?',
                                    [],
                                    'Admin.Notifications.Warning'
                                ),
                            ])
                        ),
                ])
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function getFilters()
    {
        return (new FilterCollection())
            ->add(
                (new Filter('id_reseller', TextType::class))
                ->setTypeOptions([
                    'required' => false,
                    'attr' => [
                        'placeholder' => $this->trans('Search ID', [], 'Admin.Actions'),
                    ],
                ])
                ->setAssociatedColumn('id_reseller')
            )
            ->add(
                (new Filter('name', TextType::class))
                ->setTypeOptions([
                    'required' => false,
                    'attr' => [
                        'placeholder' => $this->trans('Search name', [], 'Admin.Actions'),
                    ],
                ])
                ->setAssociatedColumn('name')
            )
            ->add(
                (new Filter('active', ChoiceType::class))
                ->setTypeOptions([
                    'required' => false,
                    'choices' => array('Sí' => true, 'No' => false),
                ])
                ->setAssociatedColumn('active')
            )
            ->add(
                (new Filter('actions', SearchAndResetType::class))
                ->setTypeOptions([
                    'reset_route' => 'admin_common_reset_search',
                    'reset_route_params' => [
                        'controller' => 'MarmicookReseller',
                        'action' => 'index',
                    ],
                    'redirect_route' => 'admin_marmicoc_resellers_index',
                ])
                ->setAssociatedColumn('actions')
            )
        ;
    }


    /**
     * {@inheritdoc}
     */
    protected function getGridActions()
    {
        return (new GridActionCollection())
            ->add((new LinkGridAction('export'))
                ->setName($this->trans('Export', [], 'Admin.Actions'))
                ->setIcon('cloud_download')
                ->setOptions([
                    'route' => 'admin_marmicoc_resellers_export',
                ])
            )
            ->add(
                (new SimpleGridAction('common_refresh_list'))
                ->setName($this->trans('Refrescar listado', [], 'Modules.Marmicook.Admin'))
                ->setIcon('refresh')
            )
            ->add(
                (new SimpleGridAction('common_show_query'))
                ->setName($this->trans('Mostrar consulta SQL', [], 'Modules.Marmicook.Admin'))
                ->setIcon('code')
            )
            ->add(
                (new SimpleGridAction('common_export_sql_manager'))
                ->setName($this->trans('Exportar al gestor SQL', [], 'Modules.Marmicook.Admin'))
                ->setIcon('storage')
            )
            // ->add(
            //     (new LinkGridAction('export'))
            //     ->setName($this->trans('Exportar CSV', [], 'Modules.Marmicook.Admin'))
            //     ->setIcon('cloud_download')
            //     ->setOptions([
            //         'route' => 'admin_marmicoc_members_export',
            //     ])
            // )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function getBulkActions()
    {
        return (new BulkActionCollection())
            ->add(
                (new SubmitBulkAction('update_requests_status'))
                ->setName($this->trans('Actualizar estado', [], 'Modules.Marmicook.Admin'))
                ->setOptions([
                    'submit_route' => 'admin_marmicoc_resellers_toggle_status_bulk',
                    'confirm_message' => $this->trans('Update selected items?', [], 'Modules.Marmicook.Admin'),
                ])
            )
        ;
    }
}
