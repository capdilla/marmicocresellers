<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Query;

use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Exception\ResellerConstraintException;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\ValueObject\ResellerId;

/**
 * Gets reseller for editing in Back Office
 */
class GetResellerForEditing
{
    /**
     * @var ResellerId
     */
    private $resellerId;

    /**
     * @param int $resellerId
     *
     * @throws ResellerConstraintException
     */
    public function __construct($resellerId)
    {
        $this->resellerId = new ResellerId($resellerId);
    }

    /**
     * @return ResellerId $resellerId
     */
    public function getResellerId()
    {
        return $this->resellerId;
    }
}
