<?php


namespace PrestaShop\Module\MarmicocResellers\Models;

use ObjectModel;


class ResellerModel extends ObjectModel
{

    public static $definition = [
        'table' => 'marmicoc_reseller',
        'primary' => 'id_reseller',
        'fields' => [
            'name' => ['type' => self::TYPE_STRING],
            'logo' => ['type' => self::TYPE_STRING],
            'active' => ['type' => self::TYPE_BOOL],
        ]
    ];
}
