<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\CommandHandler;

use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Command\BulkToggleResellerStatusCommand;

/**
 * Defines contract for BulkToggleResellerStatusHandler
 */
interface BulkToggleResellerStatusHandlerInterface
{
    /**
     * @param BulkToggleResellerStatusCommand $command
     */
    public function handle(BulkToggleResellerStatusCommand $command);
}
