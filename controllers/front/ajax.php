<?php

// Edit name and class according to your files, keep camelcase for class name.

use PrestaShop\Module\MarmicocResellers\Models\ResellerProductModel;

class MarmicocresellersAjaxModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {

        if (Tools::getValue('secure_key') == $this->module->secure_key) {


            header('Content-Type: application/json');
            switch (Tools::getValue('action')) {
                case 'get-product-resellers':
                    die(json_encode($this->getProductResellers(Tools::getValue('id_product'))));
                    break;
                case 'get-resellers':
                    die(json_encode($this->getResellers()));
                    break;
                case 'create-product-resellers':
                    die(json_encode($this->createProductResellers(Tools::getValue('id_product'))));
                    break;
                case 'delete-product-resellers':
                    die(json_encode($this->deleteProductResellers(Tools::getValue('id'), Tools::getValue('id_product'))));
                    break;
                case 'edit-product-resellers':
                    die(json_encode($this->editProductResellers(Tools::getValue('id'), Tools::getValue('id_product'))));
                    break;

                default:
                    # code...
                    break;
            }
        }
    }

    private function getResellers()
    {

        $query = new DbQuery();

        $query->select('r.*');
        $query->from('marmicoc_reseller', 'r');
        $query->where('active = 1');

        return Db::getInstance()->executeS($query);
    }



    public function getProductResellers($id_product, $select = "*")
    {

        $query = new DbQuery();

        $query->select($select);
        $query->from('product_reseller', 'pr');
        $query->rightJoin('marmicoc_reseller', 'rs', 'rs.id_reseller = pr.id_reseller');
        $query->where('active = 1 and id_product=' . $id_product);

        return Db::getInstance()->executeS($query);
    }


    private function getOne($id_product, $id_reseller)
    {
        $query = new DbQuery();

        $query->select('*');
        $query->from('product_reseller');
        $query->where('id_product=' . $id_product . ' and id_reseller=' . $id_reseller);


        return Db::getInstance()->executeS($query);
    }


    private function validate($id_product, $id_reseller)
    {
        return count($this->getOne($id_product, $id_reseller)) == 0;
    }

    private function createProductResellers($id_product)
    {

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $inputJSON = file_get_contents('php://input');
            $input = json_decode($inputJSON, TRUE);

            if (!$this->validate((int)$id_product, (int)$input["id_reseller"])) {
                $error = 'Already exist';
                throw $error;
            }

            $resellerProductModel = new ResellerProductModel();

            $resellerProductModel->id_product = (int)$id_product;
            $resellerProductModel->id_reseller = (int)$input["id_reseller"];
            $resellerProductModel->url = $input["url"];

            $resellerProductModel->save();

            return $this->getProductResellers($id_product);
        }
    }

    private function editProductResellers($id, $id_product)
    {

        if ($_SERVER['REQUEST_METHOD'] === 'PUT') {

            $inputJSON = file_get_contents('php://input');
            $input = json_decode($inputJSON, TRUE);

            $resellerProductModel = new ResellerProductModel($id);

            if ($resellerProductModel->id_reseller != (int)$input["id_reseller"] && !$this->validate((int)$id_product, (int)$input["id_reseller"])) {
                $error = 'Already exist';

                throw $error;
            }

            $resellerProductModel->id_reseller = (int)$input["id_reseller"];
            $resellerProductModel->url = $input["url"];

            $resellerProductModel->save();

            return $this->getProductResellers($id_product);
        }
    }

    private function deleteProductResellers($id, $id_product)
    {

        if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {

            $resellerProductModel = new ResellerProductModel($id);
            $resellerProductModel->delete();

            return $this->getProductResellers($id_product);
        }
    }
}
