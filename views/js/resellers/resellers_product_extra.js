(() => {
  const ACTIONS = {
    SET_DATA: "SET_DATA",
    SET_RESELLERS: "SET_RESELLERS",
    SET_RESELLERS_PRODUCT: "SET_RESELLERS_PRODUCT",
  };

  function reducer(action, state = []) {
    switch (action.type) {
      case ACTIONS.SET_DATA:
        return {
          ...state,
          ...action.payload,
        };
      case ACTIONS.SET_RESELLERS_PRODUCT:
        return {
          ...state,
          productResellers: action.payload,
        };
      case ACTIONS.SET_RESELLERS:
        return {
          ...state,
          resellers: action.payload,
        };
      default:
        return state;
    }
  }

  const createStore = (reducer, initialState) => {
    return {
      _subscriber: () => {},
      state: initialState,
      dispatch(action) {
        const newState = reducer(action, this.state);
        this.state = newState;
        this._subscriber(newState);
      },
      subscribe(fn) {
        this._subscriber = fn;
        fn(this.state);
      },
      getState() {
        return this.state;
      },
    };
  };

  const fetchData = (url, type = "GET", data) => {
    return new Promise((resolve, reject) => {
      $.ajax({
        url,
        type,
        headers: { "cache-control": "no-cache" },
        data: JSON.stringify(data),
        dataType: "json",
        success: function (response) {
          resolve(response);
        },
        error: function (xhr, ajaxOptions, thrownError) {
          reject();
        },
      });
    });
  };

  const initialState = {
    productResellers: [],
    resellers: [],
  };

  const TD = (text) => {
    const td = document.createElement("td");

    td.className = "data-type";
    td.innerHTML = text;

    return td;
  };

  const ActionsButtons = (data, onEdit, onDelete) => {
    const td = document.createElement("td");
    td.className = "action-type";
    const divContainer = document.createElement("div");
    divContainer.className = "btn-group-action text-right";

    const divGroup = document.createElement("div");
    divGroup.className = "btn-group";

    const editBtn = document.createElement("a");
    editBtn.className = "btn tooltip-link js-link-row-action dropdown-item";
    editBtn.innerHTML = ' <i class="material-icons">edit</i>';
    editBtn.onclick = function () {
      onEdit(data);
    };

    const deleteBtn = document.createElement("a");
    deleteBtn.className = "btn tooltip-link js-link-row-action dropdown-item";
    deleteBtn.innerHTML = '<i class="material-icons">delete</i>';
    deleteBtn.onclick = function () {
      confirm("Seguro que quieres eliminar este registro?");
      onDelete(data.id);
    };

    divGroup.append(editBtn);
    divGroup.append(deleteBtn);
    divContainer.append(divGroup);
    td.append(divContainer);

    return td;
  };

  const Row = (data, onEdit, onDelete) => {
    const tr = document.createElement("tr");

    tr.appendChild(TD(data.name));

    tr.appendChild(TD(`<a href="${data.url}" target=_blank >${data.url}</a>`));

    tr.appendChild(ActionsButtons(data, onEdit, onDelete));

    return tr;
  };

  const Form = (data, resellers) => {
    const options = resellers.reduce(
      (acc, curr) =>
        acc +
        `<option ${
          data && data.id_reseller == curr.id_reseller && "selected"
        }  value="${curr.id_reseller}">${curr.name}</option>`,
      ""
    );

    return `
    <div
    id="form-reseller-product"
    class="row justify-content-center"
    style="width: 40vw;"
  >
    <div class="col">
      <form class="form-horizontal" id="modal-form"  onsubmit="return false">
        <div class="card">
          <h3 class="card-header">
            <i class="material-icons">store</i>
            ${data ? "Editar" : "Agregar nuevo revendedor a este producto"}
            
          </h3>
          <div class="card-block row">
            <div class="card-text">
              <div class="form-group row">
                <label for="reseller_name" class="form-control-label">
                  <span class="text-danger">*</span>
                  Revendedor
                </label>
  
                <div class="col-sm">
                  <select value="${
                    data && data.id_reseller
                  }" class="form-control" name="id_reseller" style="width: 100%;">${options}</select>
                </div>
              </div>
              <div class="form-group row">
                <label for="reseller_name" class="form-control-label">
                  <span class="text-danger">*</span>
                  URL
                </label>
  
                <div class="col-sm">
                  <input
                    type="text"
                    name="url"
                    required="required"
                    class="form-control"
                    value="${(data && data.url) || ""}" 
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <a href="" class="btn btn-outline-secondary">
            Cancelar
          </a>
          <button class="btn btn-primary float-right" id="modal-onSave">
            Guardar
          </button>
        </div>
      </form>
    </div>
  </div>
  
  
  `;
  };

  $("#marmicoc_resellers").ready(function () {
    const url = $("#marmicoc_resellers").attr("ajax-link");

    const store = createStore(reducer, initialState);

    //init function
    function getData() {
      const urls = [
        `${url}&action=get-product-resellers`,
        `${url}&action=get-resellers`,
      ];

      const result = Promise.all(urls.map((u) => fetchData(u)));

      result.then(([productResellers, resellers]) => {
        store.dispatch({
          type: ACTIONS.SET_DATA,
          payload: {
            productResellers,
            resellers,
          },
        });
      });
    }
    getData();

    function openFancyBox(data) {
      $.fancybox.open({
        content: Form(data, store.getState().resellers),
        closeClick: false,
        height: "100",
        width: 800,
        helpers: {
          overlay: {
            closeClick: false,
          },
        },
      });
      $("#modal-onSave").click(function () {
        const jsonData = $("#modal-form")
          .serializeArray()
          .reduce((acc, curr) => ({ ...acc, [curr.name]: curr.value }), {});

        if (data) {
          return saveEdit(data.id, jsonData);
        }
        saveNew(jsonData);
      });
    }

    function saveNew(data) {
      fetchData(`${url}&action=create-product-resellers`, "POST", data)
        .then((response) => {
          store.dispatch({
            type: ACTIONS.SET_RESELLERS_PRODUCT,
            payload: response,
          });

          $.fancybox.close();
          $.growl.notice({
            title: "Guardado con exito",
            size: "large",
            message: "Cambios guardados",
          });
        })
        .catch(() => {
          $.growl.error({
            title: "Ha ocurrido un erro",
            size: "large",
            message: "los cambios no se han podido guardar.",
          });
        });
    }

    function saveEdit(id, data) {
      fetchData(`${url}&action=edit-product-resellers&id=${id}`, "PUT", data)
        .then((response) => {
          store.dispatch({
            type: ACTIONS.SET_RESELLERS_PRODUCT,
            payload: response,
          });

          $.fancybox.close();
          $.growl.notice({
            title: "Guardado con exito",
            size: "large",
            message: "Cambios guardados",
          });
        })
        .catch(() => {
          $.growl.error({
            title: "Ha ocurrido un erro",
            size: "large",
            message: "los cambios no se han podido guardar.",
          });
        });
    }

    function openEdit(data) {
      openFancyBox(data);
    }

    function onDelete(id) {
      fetchData(`${url}&action=delete-product-resellers&id=${id}`, "DELETE")
        .then((response) => {
          store.dispatch({
            type: ACTIONS.SET_RESELLERS_PRODUCT,
            payload: response,
          });
          $.growl.notice({
            title: "Guardado con exito",
            size: "large",
            message: "Cambios guardados",
          });
        })
        .catch(() => {
          $.growl.error({
            title: "Ha ocurrido un erro",
            size: "large",
            message: "los cambios no se han podido guardar.",
          });
        });
    }

    $("#add-new-product-reseller").click(function () {
      openFancyBox();
    });

    store.subscribe(function (state) {
      $("#reseller_product_table tbody").empty();
      const rows = state.productResellers.map((reseller) =>
        Row(reseller, openEdit, onDelete)
      );
      $("#reseller_product_table tbody").append(rows);
    });
  });
})();
