<?php
/**
 * @author    Alejandro del Valle <a.delvalle@softec.cloud>
 * @copyright 2018 Marmicoc
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */

namespace PrestaShop\Module\MarmicocResellers\Adapter\Reseller;

use HelperList;
use ImageManager;
use PrestaShop\PrestaShop\Core\Image\ImageProviderInterface;
use PrestaShop\PrestaShop\Core\Image\Parser\ImageTagSourceParserInterface;

/**
 * Provides path for reseller logo thumbnail
 */
final class ResellerLogoThumbnailProvider implements ImageProviderInterface
{
    const MARMICOOK_RESELLER_IMG_DIR = _PS_MODULE_DIR_ . 'marmicoc/views/img/r/';

    /**
     * @var ImageTagSourceParserInterface
     */
    private $imageTagSourceParser;

    /**
     * @var int
     */
    private $contextShopId;

    /**
     * @param ImageTagSourceParserInterface $imageTagSourceParser
     * @param int $contextShopId
     */
    public function __construct(
        ImageTagSourceParserInterface $imageTagSourceParser,
        $contextShopId
    ) {
        $this->imageTagSourceParser = $imageTagSourceParser;
        $this->contextShopId = $contextShopId;
    }

    /**
     * {@inheritdoc}
     */
    public function getPath($resellerId)
    {
        $pathToImage = self::MARMICOOK_RESELLER_IMG_DIR . $resellerId . '.jpg';

        $imageTag = ImageManager::thumbnail(
            $pathToImage,
            'reseller_mini_' . $resellerId . '_' . $this->contextShopId . '.jpg',
            HelperList::LIST_THUMBNAIL_SIZE
        );

        return $this->imageTagSourceParser->parse($imageTag);
    }
}
