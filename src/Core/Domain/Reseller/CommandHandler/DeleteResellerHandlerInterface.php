<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\CommandHandler;

use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Command\DeleteResellerCommand;

/**
 * Defines contract for DeleteResellerHandler
 */
interface DeleteResellerHandlerInterface
{
    /**
     * @param DeleteResellerCommand $command
     */
    public function handle(DeleteResellerCommand $command);
}
