{if count($resellers)}

<div class="uk-margin-top uk-margin-bottom">
  <a href="#modal-sections" uk-toggle>¿Donde comprar?</a>

  <div id="modal-sections" uk-modal>
    <div class="uk-modal-dialog">
      <button class="uk-modal-close-default" type="button" uk-close></button>
      <div class="uk-modal-header">
        <h2 class="uk-modal-title">Revendedores</h2>
      </div>
      <div class="uk-modal-body">
        <div class="uk-child-width-1-2@m" uk-grid>
          {foreach from=$resellers item=reseller key=key}
          <a href="{$reseller.url}" target="_blank">
            <div class="uk-card uk-card-default">
              <div class="uk-card-media-top">
                <img src="{$reseller.logo}" alt="" />
              </div>
              <div class="uk-card-body">
                <h3 class="uk-card-title">{$reseller.name}</h3>
                <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p> -->
              </div>
            </div>
          </a>
          {/foreach}
        </div>
      </div>
    </div>
  </div>
</div>
{/if}
