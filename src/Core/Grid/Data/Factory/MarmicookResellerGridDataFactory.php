<?php
/**
 * @author    Alejandro del Valle <a.delvalle@softec.cloud>
 * @copyright 2018 Marmicoc
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */

namespace PrestaShop\Module\MarmicocResellers\Core\Grid\Data\Factory;

use PrestaShop\PrestaShop\Core\Grid\Data\GridData;
use PrestaShop\PrestaShop\Core\Grid\Record\RecordCollection;
use PrestaShop\PrestaShop\Core\Grid\Search\SearchCriteriaInterface;
use PrestaShop\PrestaShop\Core\Image\ImageProviderInterface;
use PrestaShop\PrestaShop\Core\Grid\Data\Factory\GridDataFactoryInterface;

/**
 * Gets data for reseller grid
 */
final class MarmicookResellerGridDataFactory implements GridDataFactoryInterface
{
    /**
     * @var GridDataFactoryInterface
     */
    private $resellerDataFactory;

    /**
     * @var ImageProviderInterface
     */
    private $resellerLogoThumbnailProvider;

    /**
     * @param GridDataFactoryInterface $resellerDataFactory
     * @param ImageProviderInterface $resellerLogoThumbnailProvider
     */
    public function __construct(
        GridDataFactoryInterface $resellerDataFactory,
        ImageProviderInterface $resellerLogoThumbnailProvider
    ) {
        $this->resellerDataFactory = $resellerDataFactory;
        $this->resellerLogoThumbnailProvider = $resellerLogoThumbnailProvider;
    }

    /**
     * {@inheritdoc}
     */
    public function getData(SearchCriteriaInterface $searchCriteria)
    {
        $resellerData = $this->resellerDataFactory->getData($searchCriteria);

        $modifiedRecords = $this->applyModification(
            $resellerData->getRecords()->all()
        );

        return new GridData(
            new RecordCollection($modifiedRecords),
            $resellerData->getRecordsTotal(),
            $resellerData->getQuery()
        );
    }

    /**
     * @param array $resellers
     *
     * @return array
     */
    private function applyModification(array $resellers)
    {
        foreach ($resellers as $i => $reseller) {
            $resellers[$i]['logo'] = $this->resellerLogoThumbnailProvider->getPath(
                $reseller['id_reseller']
            );
        }

        return $resellers;
    }
}
