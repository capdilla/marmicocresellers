<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Command;

use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Exception\ResellerConstraintException;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\ValueObject\ResellerId;

/**
 * Toggles reseller status in bulk action
 */
class BulkToggleResellerStatusCommand
{
    /**
     * @var bool
     */
    private $expectedStatus;

    /**
     * @var ResellerId[]
     */
    private $resellerIds;

    /**
     * @param int[] $resellerIds
     * @param bool $expectedStatus
     *
     * @throws ResellerConstraintException
     * @throws ResellerConstraintException
     */
    public function __construct(array $resellerIds, $expectedStatus)
    {
        $this->assertIsBool($expectedStatus);
        $this->expectedStatus = $expectedStatus;
        $this->setResellerIds($resellerIds);
    }

    /**
     * @return bool
     */
    public function getExpectedStatus()
    {
        return $this->expectedStatus;
    }

    /**
     * @return ResellerId[]
     */
    public function getResellerIds()
    {
        return $this->resellerIds;
    }

    /**
     * @param int[] $resellerIds
     *
     * @throws ResellerConstraintException
     */
    private function setResellerIds(array $resellerIds)
    {
        foreach ($resellerIds as $resellerId) {
            $this->resellerIds[] = new ResellerId($resellerId);
        }
    }

    /**
     * Validates that value is of type boolean
     *
     * @param $value
     *
     * @throws ResellerConstraintException
     */
    private function assertIsBool($value)
    {
        if (!is_bool($value)) {
            throw new ResellerConstraintException(
                sprintf('Status must be of type bool, but given %s', var_export($value, true)),
                ResellerConstraintException::INVALID_STATUS
            );
        }
    }
}
