<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\CommandHandler;

use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Command\BulkDeleteResellerCommand;

/**
 * Defines contract for BulkDeleteResellerHandler
 */
interface BulkDeleteResellerHandlerInterface
{
    /**
     * @param BulkDeleteResellerCommand $command
     */
    public function handle(BulkDeleteResellerCommand $command);
}
