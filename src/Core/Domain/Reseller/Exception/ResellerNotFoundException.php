<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Exception;

/**
 * Is thrown when reseller is not found in Reseller subdomain
 */
class ResellerNotFoundException extends ResellerException
{
}
