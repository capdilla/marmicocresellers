<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Exception;

/**
 * Is thrown when cannot update reseller
 */
class UpdateResellerException extends ResellerException
{
    /**
     * When fails to update single reseller status
     */
    const FAILED_UPDATE_STATUS = 10;

    /**
     * When fails to update resellers status in bulk action
     */
    const FAILED_BULK_UPDATE_STATUS = 20;
}
