<?php

namespace PrestaShop\Module\MarmicocResellers\Adapter\Reseller\QueryHandler;

use ImageManager;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Query\GetResellerForEditing;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\QueryHandler\GetResellerForEditingHandlerInterface;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\QueryResult\EditableReseller;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\ValueObject\ResellerId;
use PrestaShop\PrestaShop\Core\Image\Parser\ImageTagSourceParserInterface;

/**
 * Handles query which gets reseller for editing
 */
final class GetResellerForEditingHandler implements GetResellerForEditingHandlerInterface
{

    const MARMICOOK_RESELLER_IMG_DIR = _PS_MODULE_DIR_ . 'marmicoc/views/img/r/';

    /**
     * @var ImageTagSourceParserInterface
     */
    private $imageTagSourceParser;

    /**
     * @var int
     */
    private $contextShopId;

    public function __construct(
        ImageTagSourceParserInterface $imageTagSourceParser,
        $contextShopId
    ) {
        $this->imageTagSourceParser = $imageTagSourceParser;
        $this->contextShopId = $contextShopId;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(GetResellerForEditing $query)
    {
        $resellerId = $query->getResellerId();
        $reseller = $this->getReseller($resellerId);

        return new EditableReseller(
            $resellerId,
            $reseller->name,
            $reseller->active,
            $this->getLogoImage($resellerId),
            $reseller->getAssociatedShops()
        );
    }

    /**
     * @param ResellerId $resellerId
     *
     * @return array|null
     */
    private function getLogoImage(ResellerId $resellerId)
    {
        $pathToImage = self::MARMICOOK_RESELLER_IMG_DIR . $resellerId->getValue() . '.jpg';
        $imageTag = ImageManager::thumbnail(
            $pathToImage,
            'reseller_' . $resellerId->getValue() . '_' . $this->contextShopId . '.jpg',
            350,
            'jpg',
            true,
            true
        );

        $imageSize = file_exists($pathToImage) ? filesize($pathToImage) / 1000 : '';

        if (empty($imageTag) || empty($imageSize)) {
            return null;
        }

        return [
            'size' => sprintf('%skB', $imageSize),
            'path' => $this->imageTagSourceParser->parse($imageTag),
        ];
    }
}
