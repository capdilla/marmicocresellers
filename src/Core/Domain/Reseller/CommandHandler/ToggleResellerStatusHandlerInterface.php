<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\CommandHandler;

use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Command\ToggleResellerStatusCommand;

/**
 * Defines contract for ToggleResellerStatusHandler
 */
interface ToggleResellerStatusHandlerInterface
{
    /**
     * @param ToggleResellerStatusCommand $command
     */
    public function handle(ToggleResellerStatusCommand $command);
}
