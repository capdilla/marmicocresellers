<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Exception;

/**
 * Is thrown when Reseller constraint is violated
 */
class ResellerConstraintException extends ResellerException
{
    /**
     * When reseller id is not valid
     */
    const INVALID_ID = 10;

    /**
     * When reseller status is not valid
     */
    const INVALID_STATUS = 20;
}
