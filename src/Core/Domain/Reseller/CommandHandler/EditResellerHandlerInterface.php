<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\CommandHandler;

use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Command\EditResellerCommand;

/**
 * Defines contract for EditResellerHandler
 */
interface EditResellerHandlerInterface
{
    /**
     * @param EditResellerCommand $command
     */
    public function handle(EditResellerCommand $command);
}
