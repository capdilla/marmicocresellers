<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\QueryHandler;

use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Query\GetResellerForEditing;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\QueryResult\EditableReseller;

/**
 * Defines contract for GetResellerForEditingHandler
 */
interface GetResellerForEditingHandlerInterface
{
    /**
     * @param GetResellerForEditing $query
     *
     * @return EditableReseller
     */
    public function handle(GetResellerForEditing $query);
}
