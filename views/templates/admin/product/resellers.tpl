<div id="marmicoc_resellers" ajax-link="{$ajax_link}">
  <a
    class="btn btn-primary pointer"
    id="add-new-product-reseller"
    href="#"
    title="Añadir nuevo"
  >
    <i class="material-icons">add_circle_outline</i> Añadir nuevo
  </a>
  <table class="grid-table js-grid-table table" id="reseller_product_table">
    <thead class="thead-default">
      <tr class="column-headers">
        <th scope="col">
          <div
            class="ps-sortable-column"
            data-sort-col-name="name"
            data-sort-prefix=""
          >
            <span role="columnheader">Nombre</span>
            <span role="button" class="ps-sort" aria-label="Ordenar por"></span>
          </div>
        </th>

        <th scope="col">
          <div
            class="ps-sortable-column"
            data-sort-col-name="name"
            data-sort-prefix=""
          >
            <span role="columnheader">URL</span>
            <span role="button" class="ps-sort" aria-label="Ordenar por"></span>
          </div>
        </th>

        <th scope="col">
          <div class="grid-actions-header-text">
            <span>Acciones</span>
          </div>
        </th>
      </tr>

      <!-- <tr class="column-filters">
        <td>
          <input
            type="text"
            id="reseller_name"
            name="reseller[name]"
            placeholder="Buscar por nombre"
            class="form-control"
          />
        </td>

        <td>
          <input
            type="text"
            name="reseller[name]"
            placeholder="Buscar por url"
            class="form-control"
          />
        </td>

        <td>
          <button
            type="submit"
            class="btn btn-primary grid-search-button d-block float-right"
            title="Buscar"
            name="reseller[actions][search]"
          >
            <i class="material-icons">search</i>
            Buscar
          </button>
        </td>
      </tr> -->
    </thead>
    <tbody></tbody>
  </table>
</div>
