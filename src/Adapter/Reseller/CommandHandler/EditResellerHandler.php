<?php

namespace PrestaShop\Module\MarmicocResellers\Adapter\Reseller\CommandHandler;

use Manufacturer;
use PrestaShop\Module\MarmicocResellers\Adapter\Reseller\CommandHandler\AbstractManufacturerCommandHandler;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Command\EditResellerCommand;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\CommandHandler\EditResellerHandlerInterface;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Exception\ResellerException;
use PrestaShopException;

/**
 * Handles command which edits manufacturer using legacy object model
 */
final class EditResellerHandler extends AbstractManufacturerCommandHandler implements EditResellerHandlerInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws ManufacturerException
     */
    public function handle(EditManufacturerCommand $command)
    {
        $manufacturerId = $command->getManufacturerId();
        $manufacturer = $this->getManufacturer($manufacturerId);
        $this->populateManufacturerWithData($manufacturer, $command);

        try {
            if (false === $manufacturer->validateFields(false)) {
                throw new ManufacturerException('Manufacturer contains invalid field values');
            }

            if (null !== $command->getAssociatedShops()) {
                $this->associateWithShops($manufacturer, $command->getAssociatedShops());
            }

            if (!$manufacturer->update()) {
                throw new ManufacturerException(
                    sprintf('Cannot update manufacturer with id "%s"', $manufacturer->id)
                );
            }
        } catch (PrestaShopException $e) {
            throw new ManufacturerException(
                sprintf('Cannot update manufacturer with id "%s"', $manufacturer->id)
            );
        }
    }

    /**
     * Populates Manufacturer object with given data
     *
     * @param Manufacturer $manufacturer
     * @param EditManufacturerCommand $command
     */
    private function populateManufacturerWithData(Manufacturer $manufacturer, EditManufacturerCommand $command)
    {
        if (null !== $command->getName()) {
            $manufacturer->name = $command->getName();
        }
        if (null !== $command->getLocalizedShortDescriptions()) {
            $manufacturer->short_description = $command->getLocalizedShortDescriptions();
        }
        if (null !== $command->getLocalizedDescriptions()) {
            $manufacturer->description = $command->getLocalizedDescriptions();
        }
        if (null !== $command->getLocalizedMetaDescriptions()) {
            $manufacturer->meta_description = $command->getLocalizedMetaDescriptions();
        }
        if (null !== $command->getLocalizedMetaKeywords()) {
            $manufacturer->meta_keywords = $command->getLocalizedMetaKeywords();
        }
        if (null !== $command->getLocalizedMetaTitles()) {
            $manufacturer->meta_title = $command->getLocalizedMetaTitles();
        }
        if (null !== $command->isEnabled()) {
            $manufacturer->active = $command->isEnabled();
        }
    }
}
