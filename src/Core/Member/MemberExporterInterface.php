<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Member;

use Symfony\Component\HttpFoundation\Response;

/**
 * Allow the export of a list of members.
 */
interface MemberExporterInterface
{
    /**
     * @param array $members
     *
     * @return Response
     */
    public function export(array $members);
}
