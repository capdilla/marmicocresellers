<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\ValueObject;

use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Exception\ResellerConstraintException;

/**
 * Provides reseller id
 */
class ResellerId
{
    /**
     * @var int
     */
    private $id;

    /**
     * @param int $id
     *
     * @throws ResellerConstraintException
     */
    public function __construct($id)
    {
        $this->assertIsIntegerGreaterThanZero($id);
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->id;
    }

    /**
     * Validates that the value is integer and is greater than zero
     *
     * @param $value
     *
     * @throws ResellerConstraintException
     */
    private function assertIsIntegerGreaterThanZero($value)
    {
        if (!is_int($value) || 0 >= $value) {
            throw new ResellerConstraintException(
                sprintf('Invalid reseller id "%s".', var_export($value, true))
            );
        }
    }
}
