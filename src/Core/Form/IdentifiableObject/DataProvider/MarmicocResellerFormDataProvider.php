<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Form\IdentifiableObject\DataProvider;

use PrestaShop\PrestaShop\Core\CommandBus\CommandBusInterface;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Query\GetResellerForEditing;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\QueryResult\EditableReseller;
use PrestaShop\Module\MarmicocResellers\Models\ResellerModel;
use PrestaShop\PrestaShop\Core\Form\IdentifiableObject\DataProvider\FormDataProviderInterface;
use PrestaShopObjectNotFoundException;

/**
 * Provides data for resellers add/edit forms
 */
final class MarmicocResellerFormDataProvider implements FormDataProviderInterface
{
    /**
     * @var CommandBusInterface
     */
    private $bus;

    /**
     * @var bool
     */
    private $multistoreEnabled;

    /**
     * @var int[]
     */
    private $defaultShopAssociation;

    /**
     * @param CommandBusInterface $commandBus
     * @param $multistoreEnabled
     * @param int[] $defaultShopAssociation
     */
    public function __construct(
        CommandBusInterface $commandBus,
        $multistoreEnabled,
        array $defaultShopAssociation
    ) {
        $this->commandBus = $commandBus;
        $this->multistoreEnabled = $multistoreEnabled;
        $this->defaultShopAssociation = $defaultShopAssociation;
    }

    /**
     * {@inheritdoc}
     */
    public function getData($resellerId)
    {
        /** @var EditableReseller $editableReseller */
        // $editableReseller = $this->commandBus->handle(new GetResellerForEditing((int) $resellerId));

        $resellerModel = new ResellerModel($resellerId);

        // check that the element exists in db
        if (empty($resellerModel->id)) {
            throw new PrestaShopObjectNotFoundException('Object not found');
        }

        return [
            'name' => $resellerModel->name,
            'active' => $resellerModel->active,
        ];;
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultData()
    {


        return [
            'active' => true
        ];
    }
}
