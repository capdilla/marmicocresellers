<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Exception;

/**
 * Is thrown reseller or resellers cannot be deleted
 */
class DeleteResellerException extends ResellerException
{
    /**
     * When fails to delete single reseller
     */
    const FAILED_DELETE = 10;

    /**
     * When fails to delete resellers in bulk action
     */
    const FAILED_BULK_DELETE = 20;
}
