<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Command;

use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\CommandHandler\AddResellerHandlerInterface;

/**
 * Creates reseller with provided data
 */
class AddResellerCommand implements AddResellerHandlerInterface
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    private $enabled;

    /**
     * @var array
     */
    private $shopAssociation;

    /**
     * @param string $name
     * @param bool $enabled
     * @param array $shopAssociation
     */
    public function __construct(
        $name,
        $enabled,
        array $shopAssociation
    ) {
        $this->name = $name;
        $this->enabled = $enabled;
        $this->shopAssociation = $shopAssociation;
    }


    public function handle($command)
    {

        var_dump("hola??");

    }

    public function handler(AddResellerCommand $command)
    {

        var_dump("hola??");

    }
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @return array
     */
    public function getShopAssociation()
    {
        return $this->shopAssociation;
    }
}
