<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\QueryResult;

use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\ValueObject\ResellerId;

/**
 * Transfers reseller data for editing
 */
class EditableReseller
{
    /**
     * @var ResellerId
     */
    private $resellerId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var array
     */
    private $logoImage;

    /**
     * @var bool
     */
    private $enabled;

    /**
     * @var array
     */
    private $associatedShops;

    /**
     * @param ResellerId $resellerId
     * @param string $name
     * @param bool $enabled
     * @param array|null $logoImage
     * @param array $associatedShops
     */
    public function __construct(
        ResellerId $resellerId,
        $name,
        $enabled,
        $logoImage,
        array $associatedShops
    ) {
        $this->resellerId = $resellerId;
        $this->name = $name;
        $this->logoImage = $logoImage;
        $this->enabled = $enabled;
        $this->associatedShops = $associatedShops;
    }

    /**
     * @return ResellerId
     */
    public function getResellerId()
    {
        return $this->resellerId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getLogoImage()
    {
        return $this->logoImage;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @return array
     */
    public function getAssociatedShops()
    {
        return $this->associatedShops;
    }
}
