<?php

namespace PrestaShop\Module\MarmicocResellers\Adapter\Reseller\CommandHandler;

use PrestaShop\Module\MarmicocResellers\Entity\MarmicookReseller;
use PrestaShop\PrestaShop\Adapter\Domain\AbstractObjectModelHandler;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Command\AddResellerCommand;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\CommandHandler\AddResellerHandlerInterface;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Exception\ResellerException;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\ValueObject\ResellerId;

/**
 * Handles command which adds new reseller using legacy object model
 */
final class AddResellerHandler extends AbstractObjectModelHandler implements AddResellerHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function handle(AddResellerCommand $command)
    {
        $reseller = new MarmicookReseller();

        $reseller->name = $command->getName();
        $reseller->active = $command->isEnabled();

        try {
            if (false === $manufacturer->validateFields(false)) {
                throw new ManufacturerException('Manufacturer contains invalid field values');
            }

            if (!$manufacturer->add()) {
                throw new ManufacturerException(
                    sprintf('Failed to add new manufacturer "%s"', $command->getName())
                );
            }
            $this->addShopAssociation($manufacturer, $command);
        } catch (\PrestaShopException $e) {
            throw new ManufacturerException(
                sprintf('Failed to add new manufacturer "%s"', $command->getName())
            );
        }

        return new ManufacturerId((int) $manufacturer->id);
    }

    /**
     * Add manufacturer and shop association
     *
     * @param Manufacturer $manufacturer
     * @param AddManufacturerCommand $command
     *
     * @throws \PrestaShopDatabaseException
     */
    private function addShopAssociation(Manufacturer $manufacturer, AddManufacturerCommand $command)
    {
        $this->associateWithShops(
            $manufacturer,
            $command->getShopAssociation()
        );
    }

    /**
     * @param Manufacturer $manufacturer
     * @param AddManufacturerCommand $command
     */
    private function fillLegacyManufacturerWithData(Manufacturer $manufacturer, AddManufacturerCommand $command)
    {
        $manufacturer->name = $command->getName();
        $manufacturer->short_description = $command->getLocalizedShortDescriptions();
        $manufacturer->description = $command->getLocalizedDescriptions();
        $manufacturer->meta_title = $command->getLocalizedMetaTitles();
        $manufacturer->meta_description = $command->getLocalizedMetaDescriptions();
        $manufacturer->meta_keywords = $command->getLocalizedMetaKeywords();
        $manufacturer->active = $command->isEnabled();
    }
}
