<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\CommandHandler;

use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Command\AddResellerCommand;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\ValueObject\ResellerId;

/**
 * Defines contract for AddResellerHandler
 */
interface AddResellerHandlerInterface
{
    /**
     * @param AddResellerCommand $command
     *
     * @return ResellerId
     */
    public function handle(AddResellerCommand $command);
}
