<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Form\IdentifiableObject\DataHandler;

use PrestaShop\PrestaShop\Core\CommandBus\CommandBusInterface;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Command\AddResellerCommand;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Command\EditResellerCommand;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\ValueObject\ResellerId;
use PrestaShop\Module\MarmicocResellers\Models\ResellerModel;
use PrestaShop\PrestaShop\Core\Image\Uploader\ImageUploaderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use PrestaShop\PrestaShop\Core\Form\IdentifiableObject\DataHandler\FormDataHandlerInterface;

/**
 * Handles submitted reseller form data
 */
final class MarmicocResellerFormDataHandler implements FormDataHandlerInterface
{
    /**
     * @var CommandBusInterface
     */
    private $commandBus;
    /**
     * @var ImageUploaderInterface
     */
    private $imageUploader;

    /**
     * @param CommandBusInterface $commandBus
     * @param ImageUploaderInterface $imageUploader
     */
    public function __construct(
        CommandBusInterface $commandBus,
        ImageUploaderInterface $imageUploader
    ) {
        $this->commandBus = $commandBus;
        $this->imageUploader = $imageUploader;
    }


    private function saveData(array $data, $resellerId = null)
    {

        $reseller = null;
        if ($resellerId) {
            $reseller =  new ResellerModel($resellerId);
        } else {
            $reseller =  new ResellerModel();
        }


        $reseller->name = $data["name"];

        /** @var UploadedFile $uploadedFlagImage */


        $logo = "";

        $reseller->logo = $logo;

        $reseller->active = $data["active"];

        $reseller->save();

        //save the image
        $uploadedLogo = $data['logo'];
        if ($uploadedLogo instanceof UploadedFile) {
            $logo = $this->imageUploader->upload($reseller->id, $uploadedLogo);
        }

        $reseller->logo = $logo;

        $reseller->save();

        return $reseller->id;
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        return $this->saveData($data);
    }

    /**
     * {@inheritdoc}
     */
    public function update($resellerId, array $data)
    {

        return $this->saveData($data, $resellerId);
    }
}
