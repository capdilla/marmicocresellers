<?php

namespace PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Command;

use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Exception\ResellerConstraintException;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\ValueObject\ResellerId;

/**
 * Deletes reseller
 */
class DeleteResellerCommand
{
    /**
     * @var ResellerId
     */
    private $resellerId;

    /**
     * @param int $resellerId
     *
     * @throws ResellerConstraintException
     */
    public function __construct($resellerId)
    {
        $this->resellerId = new ResellerId($resellerId);
    }

    /**
     * @return ResellerId
     */
    public function getResellerId()
    {
        return $this->resellerId;
    }
}
