<?php

/**
 * @author    Alejandro del Valle <a.delvalle@softec.cloud>
 * @copyright Marmicoc
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */

namespace PrestaShop\Module\MarmicocResellers\Controller\Admin;

use PrestaShop\Module\MarmicocResellers\Core\Grid\Definition\Factory\MarmicookResellerGridDefinitionFactory;
use PrestaShop\Module\MarmicocResellers\Core\Search\Filters\MarmicookResellerFilters;
use PrestaShop\Module\MarmicocResellers\Core\Domain\Reseller\Query\GetResellerForEditing;
use PrestaShop\Module\MarmicocResellers\Adapter\ResellerLogoThumbnailProvider;
use PrestaShop\Module\MarmicocResellers\Core\Form\IdentifiableObject\DataProvider\MarmicocResellerFormDataProvider;
use PrestaShop\Module\MarmicocResellers\Models\ResellerModel;
use PrestaShop\PrestaShop\Core\Exception\DatabaseException;
use PrestaShop\PrestaShop\Core\Form\FormHandlerInterface;
use PrestaShop\PrestaShop\Core\Grid\Search\SearchCriteria;
use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use PrestaShopBundle\Security\Annotation\AdminSecurity;
use PrestaShopBundle\Security\Annotation\ModuleActivated;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Responsible of "Club Marmicook > Resellers" page display.
 *
 * @ModuleActivated(moduleName="marmicocresellers", redirectRoute="admin_module_manage")
 */
class MarmicookResellerController extends FrameworkBundleAdminController
{
    /**
     * Displays the Marmicook main page.
     *
     * @param MarmicookResellerFilters $filters
     *
     * @return Response
     */
    public function indexAction(Request $request, MarmicookResellerFilters $filters)
    {

        $toolbarButtons = [
            'add_reseller' => [
                'href' => $this->generateUrl('admin_marmicoc_resellers_create'),
                'desc' => $this->trans('Add new', 'Admin.Actions'),
                'icon' => 'add_circle_outline',
            ],
        ];

        $resellerGridFactory = $this->get('prestashop.module.marmicoc.core.grid.reseller_grid_factory');
        $resellerGrid = $resellerGridFactory->getGrid($filters);

        $resellerKpiRowFactory = $this->get('prestashop.module.marmicocresellers.core.kpi_row.factory.resellers');

        return $this->render('@Modules/marmicocresellers/views/Prestashop/Admin/Reseller/index.html.twig', [
            'layoutHeaderToolbarBtn' => $toolbarButtons,
            'layoutTitle' => $this->trans('Revendedores', 'Modules.Marmicook.Admin', []),
            'requireBulkActions' => true,
            'showContentHeader' => true,
            'requireFilterStatus' => false,
            'resellersGrid' => $this->presentGrid($resellerGrid),
            'resellerKpiRow' => $resellerKpiRowFactory->build(),
            'enableSidebar' => true,
            'help_link' => $this->generateSidebarLink($request->attributes->get('_legacy_controller')),
        ]);
    }

    /**
     * Used for applying filtering actions.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function searchAction(Request $request)
    {
        $resellerGridDefinitionFactory = $this->get('prestashop.module.marmicoc.core.grid.definition.factory.reseller_grid_definition_factory');
        $definitionFactory = $resellerGridDefinitionFactory->getDefinition();

        $gridFilterFormFactory = $this->get('prestashop.core.grid.filter.form_factory');
        $searchParametersForm = $gridFilterFormFactory->create($definitionFactory);
        $searchParametersForm->handleRequest($request);

        $filters = [];
        if ($searchParametersForm->isSubmitted()) {
            $filters = $searchParametersForm->getData();
        }

        return $this->redirectToRoute('admin_marmicoc_resellers_index', ['filters' => $filters]);
    }

    /**
     * Show & process reseller creation.
     *
     * @AdminSecurity(
     *     "is_granted(['create'], request.get('_legacy_controller'))"
     * )
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        try {
            $resellerForm = $this->getFormBuilder()->getForm();
            $resellerForm->handleRequest($request);


            $result = $this->getFormHandler()->handle($resellerForm);

            if (null !== $result->getIdentifiableObjectId()) {
                $this->addFlash('success', $this->trans('Successful creation.', 'Admin.Notifications.Success'));

                return $this->redirectToRoute('admin_marmicoc_resellers_index');
            }
        } catch (Exception $e) {
            $this->addFlash('error', $this->getErrorMessageForException($e, $this->getErrorMessages()));
        }

        return $this->render('@Modules/marmicocresellers/views/Prestashop/Admin/Reseller/add.html.twig', [
            'help_link' => $this->generateSidebarLink($request->attributes->get('_legacy_controller')),
            'enableSidebar' => true,
            'resellerForm' => $resellerForm->createView(),
        ]);
    }

    /**
     * Show & process reseller editing.
     *
     * @AdminSecurity(
     *     "is_granted(['update'], request.get('_legacy_controller'))"
     * )
     *
     * @param int $resellerId
     * @param Request $request
     *
     * @return Response
     */
    public function editAction(Request $request, $resellerId)
    {
        try {
            $resellerForm = $this->getFormBuilder()->getFormFor((int) $resellerId);
            $resellerForm->handleRequest($request);

            $result = $this->getFormHandler()->handleFor((int) $resellerId, $resellerForm);

            if ($result->isSubmitted() && $result->isValid()) {
                $this->addFlash('success', $this->trans('Successful update.', 'Admin.Notifications.Success'));

                return $this->redirectToRoute('admin_marmicoc_resellers_index');
            }
        } catch (Exception $e) {
            $this->addFlash('error', $this->getErrorMessageForException($e, $this->getErrorMessages()));

            if ($e instanceof ResellerNotFoundException) {
                return $this->redirectToRoute('admin_marmicoc_resellers_index');
            }
        }

        /** @var EditableReseller $editableReseller */
        $editableReseller = new ResellerModel($resellerId);

        return $this->render('@Modules/marmicocresellers/views/Prestashop/Admin/Reseller/edit.html.twig', [
            'help_link' => $this->generateSidebarLink($request->attributes->get('_legacy_controller')),
            'enableSidebar' => true,
            'resellerForm' => $resellerForm->createView(),
            'resellerName' => $editableReseller->name,
            // 'logoImage' => $editableReseller->getLogoImage(),
        ]);
    }

    /**
     * Delete reseller
     *
     * @AdminSecurity("is_granted('delete', request.get('_legacy_controller'))", redirectRoute="admin_marmicoc_resellers_index")
     * 
     *
     * @param $resellerId
     *
     * @return RedirectResponse
     */
    public function deleteAction($resellerId)
    {

        try {
            $resellerModel = new ResellerModel($resellerId);

            $resellerModel->delete();

            $this->addFlash(
                'success',
                $this->trans('Successful deletion.', 'Admin.Notifications.Success')
            );
        } catch (ResellerException $e) {
            $this->addFlash('error', $this->getErrorMessageForException($e, $this->getErrorMessages()));
        }

        return $this->redirectToRoute('admin_marmicoc_resellers_index');
    }

    /**
     * Used for update reseller status (TODO)
     *
     * @param Request $request
     * @param int $resellerId
     *
     * @throws \Exception
     *
     * @return RedirectResponse
     */
    public function toggleStatusAction(Request $request, $resellerId)
    {
        return $this->redirectToRoute('admin_marmicoc_resellers_index');
    }

    /**
     * Used for update resellers status in bulk (TODO)
     *
     * @param Request $request
     * @param array $resellers
     *
     * @throws \Exception
     *
     * @return RedirectResponse
     */
    public function toggleStatusBulkAction(Request $request)
    {
        return $this->redirectToRoute('admin_marmicoc_resellers_index');
    }

    /**
     * Used for export resellers list in csv format
     *
     * @param Request $request
     * @param array $resellers
     *
     * @throws \Exception
     *
     * @return RedirectResponse
     */
    public function exportAction(Request $request)
    {
        return $this->get('prestashop.module.marmicoc.core.reseller.csv_exporter')->export();
    }

    /**
     * @return FormHandlerInterface
     */
    private function getFormHandler()
    {
        return $this->get('prestashop.module.marmicoc.core.form.identifiable_object.handler.reseller_form_handler');
    }

    /**
     * @return FormBuilderInterface
     */
    private function getFormBuilder()
    {
        return $this->get('prestashop.module.marmicoc.core.form.identifiable_object.builder.reseller_form_builder');
    }
}
