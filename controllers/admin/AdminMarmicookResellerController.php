<?php

use Doctrine\ORM\EntityManagerInterface;

// Legacy Workaround

class AdminMarmicookResellerController extends ModuleAdminController
{
    public function displayAjaxGetKpi()
    {
        $tooltip = '';
        switch (Tools::getValue('kpi')) {
            case 'active_resellers':
                $value = self::getActiveResellers();
            break;
            default:
                $value = false;
        }

        if ($value !== false) {
            $array = array(
                'value' => $value,
                'tooltip' => $tooltip
            );
            die(json_encode($array));
        }
        die(json_encode(array('has_errors' => true)));
    }

    public static function getActiveResellers()
    {
        $db = Db::getInstance();
        $query = new DbQuery();

        $query->select('r.*');
        $query->from('marmicoc_reseller', 'r');
        $query->innerJoin('marmicoc_reseller_shop', 'rs', 'rs.id_shop = '.(int)Context::getContext()->shop->id);
        $query->where('r.active = 1');

        $activeResellers = Db::getInstance()->executeS($query);

        return count($activeResellers);
    }
}
